\documentclass[runningheads,a4paper]{llncs}

\usepackage[ngerman]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\setcounter{tocdepth}{3}
\usepackage{graphicx}
\usepackage{url}
\usepackage{etoolbox}
\apptocmd{\thebibliography}{\raggedright}{}{}

\urldef{\mail}\path{benjamin.walter@stud.h-da.de}
\newcommand{\keywords}[1]{\par\addvspace\baselineskip
\noindent\keywordname\enspace\ignorespaces#1}

\begin{document}

\mainmatter

\title{E-Government in Deutschland - Stand und Chancen}
\titlerunning{E-Government in Deutschland - Stand und Chancen}

\author{Benjamin Walter}
\authorrunning{E-Government in Deutschland - Stand und Chancen}

\institute{Hochschule Darmstadt, Fachbereich Informatik,\\
Haardtring 100, 64295 Darmstadt, Deutschland\\
\mail\\
\url{http://fbi.h-da.de}}

\toctitle{E-Government in Deutschland - Stand und Chancen}
\tocauthor{Authors' Instructions}
\maketitle


\begin{abstract}
Im Folgenden wird sich mit dem aktuellen Stand von E-Government in Deutschland und den damit verbundenen Intentionen, sowie dem Nutzen von E-Government auseinandergesetzt.
Entscheidend hierbei sind die verschiedenen Blickwinkel auf die Umsetzung von E-Government.
Auf der einen Seite soll die Umsetzung von E-Government in geltendes Recht betrachtet werden, auf der anderen Seite werden die weiteren Möglichkeiten einer Umsetzung anhand gängiger Technologien aufgezeigt.
Außerdem wird auf die verschiedenen Anwendungsgebiete für E-Government eingegangen.
Anhand eines solchen Umrisses des Themas lässt sich bereits ein Fazit ziehen,
warum E-Government in Deutschland sich nur geringer Beliebtheit erfreut und was verbessert werden kann.
\keywords{E-Government, E-Governance, Informatik und Gesellschaft}
\end{abstract}


\section{Aktueller Stand}

\subsection{Definition}
Die deutsche Bundesregierung definiert E-Government wie folgt:
\begin{quote}
\emph{\glqq E-Government ist der Einsatz elektronischer Informationstechnologien, damit Verwaltungsangebote für jedermann einfach, schnell und ortsunabhängig zugänglich sind.\grqq}
\cite{defgov}
\end{quote}
Diese Verwaltungsangebote umfassen sowohl örtliche Gemeindeaufgaben, als auch mögliche überregionale Informationsportale wie das Portal des BSI\footnote{Bundesministerium für Inneres}.\\
Besondere Kernthemen, auf die noch weiter eingegangen wird, sind \emph{DE-Mail}\footnote{Hierzu die Ausarbeitung von Herrn Schattney}
und der \emph{neue Personalausweis}\footnote{Hierzu die Ausarbeitung von Herrn Bituev}.

\subsection{Nutzung}

\begin{figure}
  \centering
	\includegraphics[width=0.7\linewidth]{Verlauf.png}
  \caption{Statistisches Bundesamt\cite{stat} Verlauf der Nutzung}
	\label{fig:SBNutz}
\end{figure}

In Abb.\ref{fig:SBNutz} sind die Daten des statistischen Bundesamtes von den Jahren 2012 bis 2015 mit einer Stichprobenmenge von \~{}150.000 Befragten aufgeführt.\cite{stat}
Als Informationssuche zählen laut statistischem Bundesamt auch die Nutzung der öffentlichen Fahrplanauskunft und das Abrufen der Öffnungszeiten der örtlichen Gemeinden.
Mit Formularen sind Angebote gelistet, die es dem Nutzer zwar ermöglichen ein Formular von vornherein online zu beziehen, bei denen die Abgabe und Bearbeitung allerdings weiterhin vor Ort zu leisten ist.
\\\\
Man sieht sehr gut, dass die Nutzerzahlen insgesamt leicht abnehmen. Außerdem ist auffällig, dass die tatsächliche aktive Nutzung der Angebote, d.h. das Ausfüllen und Zurückschicken von Formularen, weiterhin extrem gering mit \textless20\% ausfällt.
Gründe für diese Nichtnutzung finden sich in Abb.\ref{fig:SBNNutz}.

\begin{figure}
  \centering
  \includegraphics[width=0.7\linewidth]{Gruende.png}
	\caption{Statistisches Bundesamt\cite{stat} Verlauf und Gründe der nicht Nutzung}
	\label{fig:SBNNutz}
\end{figure}

Trotz der offensichtlich rückläufigen Nutzerzahlen, scheinen die Nutzungshürden zwischen 2012 und 2015 abgenommen zu haben.
Dies zeigt sich an der Entwicklung von \glqq keine Ausreichenden Kenntnisse vorhanden\grqq .
Weiterhin stehen vermehrt E-Government Angebote zur Verfügung, wie sich anhand der Entwicklung von \glqq Formularversand wird nicht angeboten\grqq $ $zeigt.
Diese Angebote umfassen nicht nur die mit dem E-Government Gesetz\cite{egg} verbundene Verpflichtung zur Bereitstellung der gängigen Informationen, sondern auch vereinzelte Online Angebote. Bekanntestes Angebot ist z.B. Elster\footnote{Elster: \url{https://www.elster.de/}} für die elektronische Steuererklärung.
Interessant anzumerken ist, dass auch die sicherheitsrelevanten Bedenken abgenommen haben.

\subsection{Gesetzeslage}
Die aktuelle Gesetzeslage hat sich mit dem E-Government Gesetz stark verändert und ausgeweitet \cite{egovg}.
Über das Gesetz haben in dritter Lesung insgesamt 25, von etwa 650, Abgeordnete abgestimmt.\cite{egovvid}
Damit war der Bundesrat nicht einmal beschlussfähig.
\\\\
Bezugnehmend auf die reduzierten Sicherheitsbedenken lässt sich sagen, dass sich die reale Lage nicht verändert hat.
Die technischen Umsetzungen haben sich auch mit dem E-Government Gesetz nicht geändert.
Lediglich wurden weitere gesetzliche Ausnahmen für z.B. \emph{DE-Mail} definiert, welche die eingesetzen Verfahren als sicher definieren \footnote{Nach \cite{egovg}Artikel 6 Absatz 5}. Diese Sicherheit ist in der Realität für solche als vertraulich eingestuften Daten durch die als Standart definierten Verfahren nicht gegeben.
Diese Umdefinierung erfolgte mit weitgreifenden Änderungen in insgesamt 28 unterschiedlichen Gesetzen.
Betroffen waren auch so sensibele Bereiche wie das Sozialgesetzbuch, aber auch das Straßenverkehrsgesetz.(Artikel 3-6 u. 24 \cite{egovg})

\subsection{Technologien}

Am interessantesten für die Realisierung des E-Government Gesetzes sind die bestehenden Technologien. Als Basis dient der \emph{neue Personalausweis}, gefolgt von der \emph{DE-Mail} als sicheres Übetragungsmedium.
Ohne auf die genaue Funktionsweise des \emph{neuen Personalausweises} einzugehen, ist es dennoch wichtig zu verstehen wofür er genutzt werden kann.
Mit dem \emph{neuen Personalausweis} ist es möglich zusätzlich zur \emph{normalen} Offlineverifizierung mit einem speziellen Lesegerät nun eine Onlineverifizierung vorzunehmen.
Gegen einen Aufpreis von jählich \~{}30 EURO kann der Personalausweis um eine digitale Signierung erweitert werden.
\\\\
Um ein \emph{DE-Mail} Konto anzulegen wird ebenfalls ein Identifikationsnachweis benötigt.
Dieser kann kann durch einen Vertreter des Anbieters erfolgen oder durch den \emph{neuen Personalausweis}.
\emph{DE-Mail} kann vom Absender signiert und verschlüsselt werden, allerdings ist dies kein Standard. Die Signierung der Nachrichten wird vom Betreiber übernommen und gewährleistet keine Ende-zu-Ende Verschlüsselung.

\section{Hauptteil}

Wie bereits eingangs erwähnt erfreut sich das E-Government Angebot der Bundesregierung keiner sonderlich großen Beliebtheit. Hierbei stellt sich die Frage nach weiteren Gründen, abgesehen von den Offensichtlichsten, wie zum Beispiel ein geringes Angebot.
Als wichtigste Ursache ist fehlende Benutzerfreundlichkeit zu nennen.
Benutzerfreundlichkeit umfasst nicht nur die Oberflächen der einzelnen Angebote und deren eigentlicher Nutzen, wobei hier mit Nutzen die tatsächlich vollständige Ersetzung eines Behördengangs gemeint ist.
Zu Benutzerfreundlichkeit gehört auch eine einheitliche, unkomplizierte Grundlage, die für alle Services genutzt wird. In der Vergangenheit wurde vermehrt versucht \emph{DE-Mail} eine solche Schlüsselrolle einzuräumen. Zum Leidwesen der großen Betreiber fand dies jedoch kaum Anklang bei der breiten Bevölkerung. Um die Beliebtheit zu erhöhen, wurde \emph{DE-Mail} dann gesetzlich zulässig für allerlei Informationstransfers ausgewiesen.
Diese umfassen auch die Übertragung besonders schützenswerter Daten wie z.B. Finanzdaten und juristische Kommunikationen.
Doch auch diese Maßnahmen änderten nichts an der Nutzung von \emph{DE-Mail}.
\\\\
Mittlerweile ist es möglich ein \emph{DE-Mail} Konto auch mit dem \emph{neuen Personalausweis} zu registrieren und ihn auch als Authentisierung zu nutzen.
\\\\
Leider ändert sich trotz der technischen Realisierbarkeit nichts an dem Fakt, dass die \emph{DE-Mail} weiterhin vom Anbieter signiert wird.
Ist eine Ende-zu-Ende Verschlüsselung gewünscht, muss diese explizit vom Nutzer eingerichtet werden und stellt eine zusätzliche Hürde dar.
\begin{quote}
\emph{Bisher sei der Einsatz von PGP\footnote{Pretty Good Privacy: Verschlüsselungsalgorithmus für Ende-zu-Ende Verschluesselung} \glqq so komplex gewesen, dass lediglich Internet-Experten davon Gebrauch machten.\grqq }
\cite{heise}
\end{quote}
Dabei könnte man auch die Signierungsfunktion des \emph{neuen Personalausweises} nutzen, und die Mails direkt beim Absender signieren.
Diese steht allerdings nur nach einem Aufpreis für den einzelnen Bürger zu Verfügung.
Zudem benötigt man noch zusätzlich ein spezielles Lesegerät. Die Anschaffung eines solchen Gerätes birgt einen Unkostenbeitrag um die 100 EURO.
Somit bleibt es bei den \~{}40\% der Nichtnutzer, die zurecht Bedenken hinsichtlich Schutz und Sicherheit ihrer persönlichen Daten haben.
\\\\
Ein weiteres Problem stellt auch die Umsetzung der bisher beschlossenen Anforderungen dar. Nach \cite{egovg} wird die Verantwortung auf die Länder übertragen und es wird keine gemeinsame Linie umgesetzt.
Viele Gemeinden sind damit nach wie vor leider nur
\begin{quote}
  \glqq Schaufenster-orientiert\grqq
  \cite{heise2}
\end{quote}
und können gar keine richtige Services anbieten.
Viele Bürger wünschen sich daher auch eine einheitliche Anlaufstelle. \cite{golemstandart}

\subsection{Ausblick}

Mit E-Government finden sich aber auch viele Chancen.
Wie in \cite{nnk} zu sehen ist, gibt es große geschätzte finanzielle Einsparpotentiale von bis zu 50\% in manchen Bereichen.
Aber nicht nur der finanzielle Nutzen spielt eine Rolle.
Gerade gegenüber dem Bürger ermöglicht E-Government große Vereinfachungen und einen enormen Mehrwert. Denkbar sind auch Angebote wie elektronische Wahlen / Abstimmungen, also auch direkte Einflussnahme auf die Politik mit elektronischen Mitteln. Dies fällt allerdings dann schon in den Bereich des E-Governance, also dem elektronischen Regieren.\footnote{Wohin das wiederrum führen kann ist in der Ausarbeitung von Herrn Grabosch zu finden}
\\\\
Als gutes Beispiel dient Estland.
In Estland kann der E-Ausweis, vergleichbar mit unserem Personalausweis, als Ticket für die öffentlichen Beförderungsmittel genutzt werden. Weiterhin kann der E-Ausweis bereits als Versicherungskarte und als Wahlberechtigung, sowie als Reisepass genutzt werden.\cite{egovend}

\section{Fazit}

E-Government bietet in vielerlei Hinsicht Möglichkeiten jeden Bürger zu unterstützen,
sei es von notwendigen Anmeldungen bei der örtlichen Gemeinde, bis hin zur Realisierung von elektronischen Wahlen.
Die Möglichkeiten sind schier grenzenlos,
bedürfen jedoch einer geordneten gesetzlichen Regelung.
Desweiteren muss dafür auch eine technisch sinnvolle Umsetzung gewählt werden.
\\\\
Faktoren wie die Benutzerfreundlichkeit spielen dann zusammen mit der Sicherheit der Daten und deren Nutzung
eine große Rolle bei der Aktzeptanz und Nutzung der E-Government Angebote.
\\\\
Die Politik hat in der Vergangenheit dafür erste Bemühungen gezeigt, doch aus aktuellen Beispielen war zu sehen,
dass diese Bemühungen nicht ausreichend durchdacht waren.
Dies hat zur Folge, dass die Angebote nur eine geringe Aktzeptanz bei der breiten Masse finden.
\\\\
Abschließend lässt sich sagen, dass die Umsetzung von E-Government und E-Governance Angeboten in Deutschland in die richtige Richtung geht,
doch die Politik bei der konkreten Umsetzung gravierende Fehler gemacht hat und weiter macht.
Einzelne Angebote wie der \emph{neue Personalausweis} ließen sich einfach mit der sicheren Variante von \emph{DE-Mail} kombinieren und würden damit ein insgesamt vereinfachten Umgang mit den E-Government Angeboten ermöglichen.

\begin{thebibliography}{12}

\bibitem{heise} Holger Bleich: De-Mail : Ende-zu-Ende-Verschlüsselung mit PGP gestartet \url{http://www.heise.de/security/meldung/De-Mail-Ende-zu-Ende-Verschluesselung-mit-PGP-gestartet-2616388.html} (Stand 21.06.2016)

\bibitem{egovvid} Bundesregierung: 234. Sitzung vom 18.04.2013: \url{http://dbtg.tv/fvid/2298993}

\bibitem{stat} Statistisches Bundesamt(2012-2015): Fachserie. 15, Wirtschaftsrechnungen. 4, Private Haushalte in der Informationsgesellschaft - Nutzung von Informations- und Kommunikationstechnologien (IKT): ZDB-ID 1234567-X

\bibitem{defgov} Die Bundesregierung: Neues E-Government-Gesetz macht viele Behördengänge überflüssig: \url{https://www.bundesregierung.de/Content/DE/Infodienst/2013/07/2013-07-26-e-government/2013-07-25-e-government.html} (Stand 21.06.2016)

\bibitem{egg} Bundesministeriums der Justiz und für Verbraucherschutz: Gesetz zur Förderung der elektronischen Verwaltung (E-Government-Gesetz - EGovG): \url{https://www.gesetze-im-internet.de/bundesrecht/egovg/gesamt.pdf} (Stand 21.06.2016)

\bibitem{egovg} Bundesministerium für Inneres: Gesetz zur Förderung der elektronischen Verwaltung sowie zur Änderung weiterer Vorschriften: \url{https://www.bmi.bund.de/SharedDocs/Downloads/DE/Themen/OED_Verwaltung/Informationsgesellschaft/egovg_verkuendung.pdf?__blob=publicationFile} (Stand 21.06.2016)

\bibitem{golemstandart} Andreas Donath: Deutsche Internetnutzer bereit zum E-Government: \url{http://www.golem.de/0303/24603.html}

\bibitem{heise2} heise-online: E-Government: Großer Markt mit großen Problemen \url{http://www.heise.de/newsticker/meldung/E-Government-Grosser-Markt-mit-grossen-Problemen-51059.html} (Stand 21.06.2016)

\bibitem{nnk} Nationaler Normenkontrollrat: E-GOVERNMENT IN DEUTSCHLAND:
VOM ABSTIEG ZUM AUFSTIEG: \url{https://www.normenkontrollrat.bund.de/Webs/NKR/Content/DE/Download/2015_11_12_gutachten_egov_2015.pdf?__blob=publicationFile&v=6}

\bibitem{egovend} woksoll: E-Government: in Deutschland in der Sackgasse?: \url{http://wk-blog.wolfgang-ksoll.de/2015/12/03/e-government-in-deutschland-in-der-sackgasse/} (Stand 22.06.2016)

\end{thebibliography}

\end{document}
