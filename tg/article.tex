\documentclass[runningheads,a4paper]{llncs}

\usepackage[ngerman]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\setcounter{tocdepth}{3}
\usepackage{graphicx}
\usepackage{url}
\usepackage{etoolbox}
\apptocmd{\thebibliography}{\raggedright}{}{}

\urldef{\mail}\path{tobias.grabosch@googlemail.com}
\newcommand{\keywords}[1]{\par\addvspace\baselineskip
\noindent\keywordname\enspace\ignorespaces#1}

\begin{document}

\mainmatter

\title{\emph{1984} im 21. Jahrhundert}

\titlerunning{\emph{1984} im 21. Jahrhundert}

\author{Tobias Grabosch}
\authorrunning{\emph{1984} im 21. Jahrhundert}

\institute{Hochschule Darmstadt, Fachbereich Informatik,\\
Haardtring 100, 64295 Darmstadt, Deutschland\\
\mail\\
\url{http://fbi.h-da.de}}

\toctitle{\emph{1984} im 21. Jahrhundert}
\tocauthor{Tobias Grabosch}
\maketitle

\begin{abstract}
Im vorliegenden Essay werden im Zusammenhang mit dem eGovernment-Gesetze das Gefahrenpotential Daten sammelnder Unternehmen, wie Techgiganten und Dienstleistern in den Finanzmärkten, mit Blick auf das in der Dystopie \emph{1984} von George Orwell gezeichnete Horrorszenario des totalen Überwachungsstaates analysiert. Auf Basis dieser Analyse werden die Machtverhältnisse von Techgiganten und Finanzmärkten im Vergleich zur Politik herausgearbeitet und eingeordnet, bevor abschließend die
aktuelle gesellschaftspolitische Lage und Herausforderungen im Lichte der vorhergehenden Erkenntnisse bewertet und kommentiert werden.
\keywords{ 1984, BigData, eGovernment-Gesetz, Datenhändler, Schufa, Techgiganten, Finanzmärkte }
\end{abstract}


\section{Von eGovernment zu \emph{1984}}
Wir schreiben das 21. Jahrhundert, das Informationszeitalter prägt mittlerweile entscheidend die
meisten aller Lebensbereiche, viele Heranwachsende kennen die Zeit vor dem alles sehenden,
alles verschlingenden 1000-armigen Technik-Krakens namens Internet gar nicht mehr und der Prozentsatz
an Menschen, die nicht mehr in der Lage sind, zu einem Leben ohne selbiges zurückzukehren, ist noch weitaus
höher, Tendenz steigend.

Verstehen Sie mich nicht falsch. Dies soll mit Nichten eine Anklageschrift sein. Nichts liegt mir ferner
als mich und meine Ansichten als höchste Wahrheit zu verkaufen und meinen Mitmenschen meine Sicht auf die
Welt aufzudrängen. Ich möchte lediglich versuchen, ein paar Fragen zu stellen und Widersprüche aufzuzeigen,
die sich mir im Laufe meines Studiums der deutschen Ausprägung des eGovernment im Zusammenhang mit anderen
politischen und wirtschaftlichen Gegebenheiten der Neuzeit aufgedrängt haben.

Bevor ich aber dazu komme, lassen Sie mich kurz den Kontext anreißen, sodass auch der nicht eingeweihte Leser
ein Fundament hat, auf dem die folgenden Ausführungen aufbauen können.

Der Europäische Rechnungshof definiert in \cite{eu} eGovernment wie folgt:

\begin{quote}
\emph{\glqq Als eGovernment bezeichnet man elektronische Geschäftsprozesse zwischen dem Staat und den Bürgern oder Unternehmen und zwischen verschiedenen öffentlichen Verwaltungen. eGovernment wird definiert als Einsatz der Informations- und Kommunikationstechnologien in Verbindung mit organisatorischen Änderungen und neuen Fähigkeiten mit dem Ziel, die öffentlichen Dienstleistungen zu verbessern\grqq}
\end{quote}

Meine Kommilitonen Arnold Bituev, Andreas Schattney und Benjamin Walter und ich hielten am 9. Juni 2016 im 
Rahmen der Lehrveranstaltung \glqq Informatik und Gesellschaft\grqq einen Vortrag zu eGovernment in Deutschland,
stellten einen Teil der bisher verfügbaren Dienste für Bürger vor und arbeiteten vor allem die 
Unzulänglichkeiten dieser Dienste heraus. Unser liebstes Angriffsziel waren die De-Mail, das eGovernment-Gesetz
und der scheinbare Dilettantismus der Politiker, der sich wie ein roter Faden durch die Thematik zog. Die
auf den Vortrag folgende Diskussion führte sogar soweit, dass die Zuhörer - angestachelt durch unsere 
subjektiv gefärbten negativen Darstellungen in der Breite, allerdings nicht in der Tiefe - anfingen,
die Integrität und Funktion von Politik, Staat und Demokratie in Frage zu stellen.

Je länger ich mich mit dieser Thematik beschäftige, desto stärker komme ich zu der Überzeugung, dass unser
Vortrag viel zu eindimensional war und sich so voreilig auf die genannten Schuldigen eingeschossen hatte,
dass die Diskussion der Zuhörer nicht anders ausfallen konnte. Das werde ich versuchen, zu beheben und 
der Schuldfrage zumindest eine weitere Dimension zu geben.

Was hat das Ganze nun mit \emph{1984} zu tun? \emph{1984} ist ein dystopischer Roman von George Orwell,
veröffentlicht 1949. Darin beschreibt Orwell einen postmodernen Überwachungsstaat, der neben modernster
Technik zur kontinuierlichen audiovisuellen Überwachung seiner Bürger durch die \emph{Gedankenpolizei}
auch die Medien einsetzt zur Manipulation einsetzt. So wird das staatliche Oberhaupt \emph{Big Brother}
als Wächter, als Beschützer der Bürger dargestellt und ein Feindbild in Form eines angeblichen, ehemaligen
Parteimitglieds \emph{Emmanuel Goldstein} kreiert. \cite{orwell}

Davon sind wir im heutigen Deutschland zwar weit entfernt, aber auch wir haben Probleme, die erst die
technische Revolution möglich machte und die Auswirkungen in diesem Bereich haben. Die Möglichkeiten, Informationen über
alles und jeden zu sammeln, zu konservieren und in einem größeren Kontext auszuwerten, sind erst in den letzten
Jahren herangewachsen. Diese Möglichkeiten bieten unter anderem jedem, der Zugriff auf die Daten bekommt, ein
ultimatives Werkzeug zur Überwachung, weshalb diese technischen Möglichkeiten den besonderen Fokus demokratischer
Kontrolle erhalten sollte, um ein Szenario wie in \emph{1984} beschrieben zu verhindern. Gerade im Bereich des
Datenschutzes, einem vitalen Teilaspekt, scheint die Politik jedoch unverständliche und vermeidbare Fehler zu machen,
weshalb die von meinen Kommilitonen und mir im Vortrag angebrachte Kritik sich fast ausschließlich auf die
Politiker fokussierte. Diese viel zu kurzsichtige Analyse möchte ich in nun korrigieren.

\section{Datenhändler}
Erste Zweifel kamen mir bei der Diskussion nach dem Vortrag meiner Gruppe, weshalb ich an dieser Stelle ansetzen möchte.
Der Vortrag war gerade zu Ende und mein Kommilitone Benjamin Walter leitete die im Anschluss an der Vortrag 
gesetzte Diskussion mit einem Beispiel ein: Die Regierung eines Entwicklungslandes, welches über eine gut ausgebaute IT-Infrastruktur
verfügt, entschließt sich, die Daten, die sie von ihren Mitarbeitern in den verschiedensten Ministerien und anderen staatlichen
Einrichtungen, wie der Wasserversorgung, haben, zu analysieren. Das Land verfügt über viele ausländische Fachkräfte, die 
im Konfliktfall mit ihrem Heimatland ausreisen könnten, was den Staat mit einem riesigen Personal- und Know-How-Problem
belasten würde. Um dieses Risiko zu vermeiden, nutzt die Regierung die mit eGovernment gesammelten Daten und ihre IT-Infrastruktur,
um Schlüsselpositionen, die von ausländischen Fachkräften besetzt sind, zu identifizieren und durch heimisches Personal zu 
ersetzen. Nun sollte die Gruppe beurteilen, wie wahrscheinlich ein solches Szenario in Deutschland ist.

Die Reaktionen fielen - zu meinem Erstaunen - sehr gutgläubig aus, vor allem wenn man die regen Diskussionen zu den Vorträgen \glqq Soziale
Netzwerke\grqq oder \glqq Gender und Informatik\grqq bei weniger brisanten Fragen betrachtet. So etwas sei nicht möglich in 
Deutschland. Es würde vielleicht eine zeit lang funktionieren, aber sobald Medien und Datenschützer es an die Öffentlichkeit
tragen würden, gäbe es einen Aufschrei, der die regierenden Parteien entmachten würde. 
Das ist grundsätzlich keine falsche Sichtweise, allerdings ist sie doch sehr naiv. Denn wer versichert uns denn, dass die
Medien darüber berichten würden. Wer sagt, dass Politik und Medien nicht eine Art symbiotischer Zusammenarbeit 
aufbauen?

Freilich kann das niemand mit Gewissheit sagen, aber logisch betrachtet gibt es wichtigere Partner für die Medienkonzerne als die Politik. 
In der freien Wirtschaft gibt es eine Reihe von Unternehmen, die längst alle Daten sowie Einfluss auf Verbraucher und/oder die
Finanzmärkte - der zentralen Macht-Ballungszentren im Kapitalismus - haben, was sie brauchen und was sie anderen verkaufen können.
Google, Facebook, Amazon, Twitter und viele mehr. Wir kaufen die Dienste dieser Unternehmen nicht mit Geld, sondern mit unseren Daten.
Kein Unternehmen dieser Welt könnte Profit aus einer kostenlosen Suchmaschine, einem kostenlosen sozialen Netzwerk, Kurznachrichtendiensten
und dergleichen schöpfen, ohne die Daten der Nutzer gewinnbringend an den meist bietenden zu verkaufen. Ein Beispiel aus der Datenrichtlinie von Facebook:

\begin{quote}
	\emph{\glqq We share information we have about you within the family of companies that are part of Facebook.\grqq} \cite{fb}
\end{quote}

Im nächsten Unterpunkt findet man:

\begin{quote}
	\emph{\glqq If the ownership or control of all or part of our Services or their assets changes, we may transfer your information to
	the new owner.\grqq} \cite{fb}
\end{quote}

Der Begriff \emph{Assets} ist dabei so allgemein, dass sich Facebook damit problemlos Informationen weiterverkaufen könnte.

Auch hier kann man natürlich argumentieren, dass die Nutzer dieser Dienste die Datenrichtlinie akzeptieren müssen, um diese nutzen zu können.
Es handle sich auch um US-Amerikanische Unternehmen, denen das deutsche Datenschutzgesetz diese Art der Datennutzung erschwere.
Ein legitimer Punkt. Der Einfluss dieser Techgiganten auf die Finanzmärkte ist gewiss, aber doch ungreifbar. Man kann sich nicht gut vorstellen,
wie diese Einflussnahme genau aussieht. Aber es gibt andere, offensichtlichere Beispiele in Deutschland selbst, die sie nicht fragen,
ob sie Ihre Daten sammeln dürfen und die Sie auf Anfrage an große Banken verkaufen.

Die Schufa ist bekanntermaßen eine der führenden Auskunfteien in Deutschland wenn es um die Kreditwürdigkeit einer Person geht. Banken können bei der Schufa
einen einen Score für eine Person anfragen, einen Prozentwert, der die geschätzte Wahrscheinlichkeit für eine Rückzahlung des Kredites darstelle, wie der Spiegel in \cite{sp2} berichtet. Wie die Schufa zu diesem Wert komme ist unbekannt. In Anbetracht der Tatsache, dass ganze Existenzen von dieser Bewertung abhängen können, ist das unbefriedigend. Der Bundesgerichtshof hat dies übrigens für gesetzeskonform erklärt, nachzulesen in \cite{lex}.
Zwar ist die Schufa mittlerweile dazu angehalten, den Verbrauchern auf Anfrage einmal kostenlos pro Jahr Auskunft über die 
von ihnen gespeicherten Daten zu geben, doch alle Daten werden laut einem Bericht von Spiegel Online \cite{sp} nicht preisgegeben.
Das Löschen von Daten geht nur, wenn bewiesen werden kann, dass die Daten falsch sind, was bei nicht herausgegebenen Daten äußerst schwierig werden dürfte.

Ist die Schufa also die Spitze des Eisberges? Nun, wenn man die Frage so stellt, ist wohl direkt ersichtlich, dass die
Antwort nur \emph{Nein} sein kann. Karten Seibel hat darüber in \cite{welt} berichtet, wobei die Informationen als
verlässlich eingestuft werden können, da sie vom damaligen Kreditech-Chef Sebastian Diemer selbst stammen:
Es gebe mittlerweile eine Reihe von Start-Ups, die die Schufa wie einen \glqq Schuljungen\grqq aussehen ließen. 
Eines dieser deutschen Unternehmen sei Kreditech. Dieses Unternehmen gewähre Mikrokredite, wobei die Entscheidung, ob
ein Kredit vergeben werde, ganz alleine der Computer fälle. Mithilfe von BigData ziehe das Unternehmen alle Daten des
Antragstellers, die es im Internet finden könne, einschließlich Facebook, Google und Twitter - diese müssten offiziell allerdings
erst vom Benutzer freigegeben werden, allerdings erhöhe die Menge der freigegebenen Daten ebenfalls die Chance auf den Kredit -,
zusammen, analysiere sie und schätze, ob der Antragsteller den Kredit wird zurückzahlen können oder nicht. Sollte es zu dem
Entschluss kommen, der Antragsteller wird den Kredit nicht zurückzahlen können, würde dieser abgelehnt.
Selbst installierte Schriftarten, Surfverhalten, Kriminalitätsrate in der Umgebung und selbst die Art und Weise wie das Formular
ausgefüllt wird - mit Copy and Paste oder ohne - spiele eine Rolle. Zwar würden die Services von Kreditech nicht in Deutschland angeboten,
auch wegen dem Datenschutzgesetz und dem Widerstand der Datenschützer, aber es zeige sehr gut die Entwicklung der Branche auf.

Dass 2013 der Bundestag das eGovernment-Gesetz mit Ausnahmeregelungen für De-Mail im Sozialgesetzbuch durchgesetzt hat,
mit der der Provider auch sensible Daten, wie Gesundheitsdaten, entschlüsseln darf, scheint das diesen Eindruck nur zu bestätigen.
Wie schnell sich der Status Quo und die Wahrnehmung der Öffentlichkeit ändern, wird besonders deutlich, wenn man sich vor Augen führt,
dass die Schufa 2012, Daten von Facebook nutzen wollte, aber aufgrund eines medialen Aufschreis gestoppt wurde. \cite{welt2}
Knapp 3 Jahre später greift Kreditech genau diese Idee auf und geht noch einen Schritt weiter, aber das mediale Echo bleibt aus. 

Die Wirtschaft hat also mit Schufa, Facebook, Google und Co. nicht nur Mittel und Wege, an so ziemlich alle Daten zu kommen. Stattdessen gibt
es bereits einen Wettbewerb, wie das Beispiel Kreditech und viele weitere Start-Ups belegen. Was ist also der Grund für das eGovernment-Gesetz,
mit all seinen Lücken? Mit der Ausnahmeregelung im zehnten Buch Sozialgesetzbuch §67 Absatz 6.3b für De-Mail und der nicht verpflichtenden Verschlüsselung von Nachrichten gegenüber dem Provider? \cite{sgb} Damit Unternehmen nun auch auf die letzten, bislang durch Gesetze wie das Datenschutzgesetz geschützte
Informationen wie die Gesundheitsdaten leichter von Unternehmen bezogen und missbraucht werden können? Haben sich die führenden Techgiganten, die Finanzmärkte
und der Staat verbündet, um die ultimative Überwachung à la \emph{1984} einzuführen und sogar auf ein globales Überwachungsnetz
auszuweiten, etwas, dass sich nicht einmal George Orwell in einem dystopischen Roman wie \emph{1984} ausgemalt hat?

\section{Und die Politik?}
Wie es auch immer sein mag, man wird es wohl kaum beweisen können. Allerdings gibt einen Punkt, der mir widersprüchlich erscheint, der sowohl in sich selbst, als auch in Verbindung mit den genannten Fakten keinen sinnvollen Schluss zulässt. Als das Gesetz im Bundestag beschlossen wurde, waren kaum noch Abgeordnete
da, weniger als 30, wie in \cite{deb} ersichtlich, womit der Bundestag nicht beschlussfähig war, denn dafür müsste laut der
Geschäftsordnung des Bundestages in zum Thema Beschlussfähigkeit in \cite{bnd} mindestens die Hälfte der 630 Abgeordneten anwesend sein. 
Die Beschlussfähigkeit kann von einer Fraktion oder mindestens von 5\% der Anwesenden angezweifelt werden, was aber nicht geschehen ist.
Man müsste nun also schließen, dass alle Parteien beziehungsweise mehr als 5\% der Anwesenden für dieses Gesetz waren, was die im 
letzten Absatz genannte These des Bündnisses unterstützen würde. Tatsächlich zeigt der Mitschnitt der betreffenden Plenarsitzung des
Bundestages in \cite{deb} aber, dass sowohl die Grünen und die Linke als auch die SPD - die in Person von Gerold Reichenbach das Gesetz mit dem Vergleich einer löchrigen Wand, die als löcherfrei gelte, weil Löcher als nicht zur Wand gehörend definiert werden, karikierten - gegen das Gesetz waren.

Warum haben sie also nicht einfach den Beschluss des Gesetzes aufgehalten, indem sie die Beschlussfähigkeit des Bundestages angezweifelt hätten?
Dieser Punkt ergibt auch nach längerer Überlegung keinen Sinn, weder zur Entlastung, noch zur Belastung der Abgeordnete.
Betrachtet man die NSA-Affäre aus den Jahren 2014 und 2015, also \emph{nach} Beschluss des eGovernment-Gesetzes, in der deutsche Politiker, Medien, Datenschützer, einfach jeder, der über medialen Einfluss verfügt, auf die Barrikaden gegangen sind, dann kann man sich gar nicht vorstellen, dass die gleichen Politiker ein beziehungsweise zwei Jahre zuvor das eGovernment-Gesetz beschlossen haben. 

\section{Spiel im Schatten}
Wie ist das alles nun zu bewerten, zu begreifen, welche Schlüsse sollte man für sich selbst und für die Zukunft ziehen? Zum Einen ist klar, 
dass nicht allein unsere demokratisch gewählten Repräsentanten in dem Maße verantwortlich sind, wie meine Gruppe und ich sie bei dem Vortrag
gemacht haben. Vielmehr haben Techgiganten und Finanzmärkte das Zepter in der Hand, die Zukunft nach ihren Vorstellungen zu gestalten.
Sie sind, um es mit Orwells Worten auszudrücken, der eigentliche \glqq Big Brother\grqq. Die Politik nimmt mehr und mehr die Rolle eines Reliktes ähnlich
den Königshäusern an. Einst mächtig, heute eher mit Symbolwirkung, aber machtlos. Ob sie sich dessen bewusst ist, bleibt abzuwarten, aber damit abfinden
werden sich wohl nicht alle Politiker, wie die Debatte zum eGovernment-Gesetz deutlich gezeigt hat.

Ich persönlich hoffe, dass die Politik, die Demokratie es schaffen wird, die Macht von Techgiganten und Finanzmärkten einzuschränken und zu alter
Größe zu wachsen. Allerdings bezweifle ich, dass das mit unserer heutigen Gesellschaftsform, mit der aktuellen Ausprägung des Kapitalismus, der
die Schere zwischen Arm und Reich immer schneller auseinander treibt, mit all den zwischenstaatlichen und -kulturellen Spannungen sowie nationalen Interessen auf dem Globus, wie dem Konflikt mit Russland, der Türkei, dem IS, dies in den nächsten Jahren auch passieren wird. Dafür sind die Herausforderungen
zu groß. Nichtsdestotrotz müssen sie angegangen werden.

Lassen Sie mich dieses Essay mit dem Refrain des Liedes \emph{Welcome to 1984} der Gruppe \emph{Anti-Flag} \cite{af} abschließen, welcher herrlich
knapp und präzise die von mir vertretene Position auf den Punkt bringt:

\begin{quotation}
	\centering
	\glqq Hell yeah, I'm confused for sure.
	
	What I thought was the New Millenium is 1984!

	Mr. Orwell from the grave, adding fresh ink to the page
	
	As the unpresident declares an endless war...
	
	Welcome to 1984!\grqq
\end{quotation}

\newpage

\begin{thebibliography}{12}
	\bibitem{orwell} Orwell, George (1949): Nineteen Eighty-Four. London: Penguin, Auflage von 2008
	\bibitem{eu} Europäischer Rechnungshof (2011): Pressemitteilung: \glqq Waren die aus dem EFRE geförderten eGovernment-Projekte wirksam?\grqq. Online unter: \url{http://europa.eu/rapid/press-release_ECA-11-34_de.htm} (Stand: 21.06.2016)
	\bibitem{fb} Datenrichtlinie von Facebook (2015). Online unter: \url{https://www.facebook.com/about/privacy/#} (Stand: 21.06.2016)
	\bibitem{sp} O.V. (2014): Kreditbewertung: Schufa teilt Verbrauchern nicht alle gespeicherten Daten mit. In: Spiegel Online, vom 10.04.2014. Online unter: \url{http://www.spiegel.de/wirtschaft/unternehmen/schufa-speichert-mehr-daten-als-gedacht-a-963661.html} (Stand: 21.06.2016)
	\bibitem{sp2} O.V. (2014): BGH zu Bonitätsbewertung: Was das Schufa-Urteil für die Verbraucher bedeutet. In: Spiegel Online, vom 28.01.2014. Online unter: \url{http://www.spiegel.de/wirtschaft/service/bgh-weist-klage-gegen-schufa-ab-was-verbraucher-wissen-muessen-a-945965.html} (Stand: 21.06.2016)
	\bibitem{lex} Pressestelle des Bundesgerichtshof (2014): Bundesgerichtshof entscheidet über Umfang einer von der SCHUFA zu erteilenden Auskunft. Online unter: \url{http://juris.bundesgerichtshof.de/cgi-bin/rechtsprechung/document.py?Gericht=bgh&Art=en&Datum=Aktuell&nr=66583&linked=pm} (Stand: 21.06.2016)	
	\bibitem{welt} Seibel, Karsten (2015): Gegen Kreditech ist die Schufa ein Schuljunge. In: Die Welt, vom 17.04.2015. Online unter: \url{http://www.welt.de/finanzen/verbraucher/article139671014/Gegen-Kreditech-ist-die-Schufa-ein-Schuljunge.html} (Stand: 21.06.2016)
	\bibitem{welt2} Hinrichs, Per (2012): Schufa will Bürger-Daten über Facebook sammeln. In: Die Welt, 07.06.2012. Online unter: \url{http://www.welt.de/finanzen/verbraucher/article106428900/Schufa-will-Buerger-Daten-ueber-Facebook-sammeln.html} (Stand 21.06.2016)
	\bibitem{sgb} §67 \glqq Begriffsbestimmungen\grqq, Zehntes Buch Sozialgesetzbuch. Online unter: \url{https://dejure.org/gesetze/SGB_X/67.html} (Stand: 21.06.2016)
	\bibitem{bnd} §45 \glqq Feststellung der Beschlussfähigkeit, Folgen der Beschlussunfähigkeit\grqq, Geschäftsordnung des Deutschen Bundestages. Online unter: \url{https://www.bundestag.de/service/glossar/B/beschl_faehig/245348} (Stand: 21.06.2016)
	\bibitem{deb} Aus der Video-Mediathek des Deutscher Bundestag: TOP 20 Förderung der elektronischen Verwaltung, 234. Plenarsitzung vom 18.04.2013. Online unter: \url{http://dbtg.tv/fvid/2298993} (Stand: 21.06.2016)
	\bibitem{af} Anti-Flag (2007): \glqq Welcome to 1984!\grqq. Liedtext online unter: \url{http://www.magistrix.de/lyrics/Anti-Flag/Welcome-To-1984-189941.html} (Stand: 21.06.2016)
\end{thebibliography}

\end{document}
